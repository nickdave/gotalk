package main

import (
	"flag"
	"fmt"
	serve "gotalk/core/server"
	"net"
	"os"
	"time"

	"github.com/golang/glog"
)

var port = "9152" // one port above tor, just for kicks
var server serve.Server

func usage() {
	fmt.Fprintf(os.Stderr, "usage: server -port=9152 -log_dir=[string] -stderrthreshold=[INFO|WARN|FATAL] \n")
	flag.PrintDefaults()
	os.Exit(2)
}

func init() {
	flag.Usage = usage
	// NOTE: This next line is key you have to call flag.Parse() for the command line
	// options or "flags" that are defined in the glog module to be picked up.
	flag.StringVar(&port, "port", "9152", "Sets the port that the server will listen on")
	flag.Parse()
}

func main() {
	server = *serve.NewServer()
	cinn, err := net.Listen("tcp", ":"+port)
	if err != nil {
		fmt.Println(err)
	}

	glog.Info(fmt.Sprintf("server started and listening on port %s", port))

	// Handling server updates
	go func() {
		for {
			server.Update()
			time.Sleep(10 * time.Microsecond) // hopefully stops the 100% CPU
		}
	}()

	for {
		conn, err := cinn.Accept()
		if err != nil {
			fmt.Println(err)
			conn.Close()
			continue
		}

		go server.HandleNewClient(conn)
	}
}

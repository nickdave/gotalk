GoTalk
======

![Logo](https://gitlab.com/trongle/gotalk/raw/master/gotalklogo.png)

Setup
------
1. You will need to open port 9152 in order to host a server

Purpose
------
 * GoTalk is an app that hopes to fix our problem of not having a reliable, encrypted chat service.
 * It focuses on user security with end to end encryption capabilities to ensure your conversation is private.
 * The server 'dumbly' distributes packets never decrypting them itself, so you can ensure no server side reading is taking place.
 * Encryption keys are only known by the users ensuring even the admin of the server cannot decipher your messages.

General Info
------------
 * GoTalk is written entirely in Go
 * The UI is written using Qt using Therecipe's QT bindings
 * We're just two dudes making this for fun, so don't expect anything extreme!
 * No matter what point in development this project reaches, we are leaving it completely open source.

Dependencies
------------
 * Google's UUID Library https://github.com/google/uuid
 * Therecipe's QT Bindings for go https://github.com/therecipe/qt
 * Golang Glog for error logging https://github.com/golang/glog
 * LibOpus wrapper for Go https://github.com/hraban/opus

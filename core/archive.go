package core

import (
	"errors"
	"fmt"

	"github.com/golang/glog"
	"github.com/google/uuid"
)

// NewArchive creates a new archive.
func NewArchive() (a *Archive) {
	id, _ := uuid.NewRandom()

	return &Archive{id,
		make(map[string]PacketHandler),
		make(map[string]ProfileHandler),
		make(map[string]*Profile, MaxClients),
		make(map[string]*Room, MaxRooms)}
}

// Archive stores rooms and clients.
type Archive struct {
	Version         uuid.UUID
	PacketHandlers  map[string]PacketHandler  `json:"-"`
	ProfileHandlers map[string]ProfileHandler `json:"-"`
	Profiles        map[string]*Profile
	Rooms           map[string]*Room
}

// UpdateVersion changes the version number.
func (a *Archive) UpdateVersion() error {
	a.Version, _ = uuid.NewRandom()
	return nil
}

// GetPacketHandler is used to retrieve a PacketHandler via a UUID.
func (a *Archive) GetPacketHandler(id string) (PacketHandler, error) {
	handler := a.PacketHandlers[id]
	if handler == nil {
		return nil, errors.New("PacketHandler does not exist")
	}

	return handler, nil
}

// GetProfileHandler is used to retrieve a ProfileHandler via a UUID.
func (a *Archive) GetProfileHandler(id string) (ProfileHandler, error) {
	handler := a.ProfileHandlers[id]
	if handler == nil {
		return nil, errors.New("ProfileHandler does not exist")
	}

	return handler, nil
}

// GetProfile is used to retrieve a Profile via a UUID.
func (a *Archive) GetProfile(id string) (*Profile, error) {
	profile := a.Profiles[id]
	if profile == nil {
		return nil, errors.New("Client does not exist")
	}
	return profile, nil
}

// GetRoom is used to retrieve a Room via a UUID.
func (a *Archive) GetRoom(id string) (*Room, error) {
	room := a.Rooms[id]
	if room == nil {
		return nil, errors.New("Room does not exist")
	}

	return room, nil
}

// AddPacketHandler is used to add a PacketHandler to the archive.
func (a *Archive) AddPacketHandler(id string, handler PacketHandler) error {
	if handler == nil {
		return errors.New("handler was nil")
	}

	if a.ProfileHandlers[id] != nil {
		return errors.New("handler with that ID already exists")
	}

	glog.Info(fmt.Sprintf("added new PacketHandler %s", id))
	a.PacketHandlers[id] = handler
	return nil
}

// AddProfileHandler is used to add a ProfileHandler to the archive.
func (a *Archive) AddProfileHandler(id string, handler ProfileHandler) error {
	if handler == nil {
		return errors.New("handler was nil")
	}

	if a.ProfileHandlers[id] != nil {
		return errors.New("handler with that ID already exists")
	}

	glog.Info(fmt.Sprintf("added new ProfileHandler %s", id))
	a.ProfileHandlers[id] = handler
	return nil
}

// AddProfile is used to add a Profile to the archive.
func (a *Archive) AddProfile(profile *Profile) error {
	if profile == nil {
		return errors.New("client was nil")
	}

	if a.Profiles[profile.UUID.String()] != nil {
		return errors.New("client with that ID already exists")
	}

	defer a.UpdateVersion()

	glog.Info(fmt.Sprintf("added new profile %s: %s", profile.UUID, profile))
	a.Profiles[profile.UUID.String()] = profile
	if profile.Client != nil {
		a.AddPacketHandler(profile.UUID.String(), profile.Client)
	}

	return nil
}

// AddRoom is used to add a Room to the archive.
func (a *Archive) AddRoom(room *Room) error {
	if room == nil {
		return errors.New("room was nil")
	}

	if a.Rooms[room.UUID.String()] != nil {
		return errors.New("room with that ID already exists")
	}

	defer a.UpdateVersion()

	glog.Info(fmt.Sprintf("added new room %s: %s", room.UUID, room))
	a.Rooms[room.UUID.String()] = room
	a.AddPacketHandler(room.UUID.String(), room)
	a.AddProfileHandler(room.UUID.String(), room)
	return nil
}

// ForceAddProfile forcibly adds a Profile to the archive, overwriting any existing data that may exist.
func (a *Archive) ForceAddProfile(profile *Profile) error {
	if profile == nil {
		return errors.New("client was nil")
	}

	defer a.UpdateVersion()

	glog.Info(fmt.Sprintf("forcibly added a profile %s: %s", profile.UUID, profile))
	a.Profiles[profile.UUID.String()] = profile
	if profile.Client != nil {
		a.AddPacketHandler(profile.UUID.String(), profile.Client)
	}

	return nil
}

// ForceAddRoom is used to forcibly add a Room to the archive, overwriting any existing data that may exist.
func (a *Archive) ForceAddRoom(room *Room) error {
	if room == nil {
		return errors.New("room was nil")
	}

	defer a.UpdateVersion()

	glog.Info(fmt.Sprintf("forcibly added a room %s: %s", room.UUID, room))
	a.Rooms[room.UUID.String()] = room
	a.AddPacketHandler(room.UUID.String(), room)
	a.AddProfileHandler(room.UUID.String(), room)
	return nil
}

// RemovePacketHandler is used to remove a PacketHandler from the archive. Returns the removed PacketHandler.
func (a *Archive) RemovePacketHandler(id string) (PacketHandler, error) {
	handler, err := a.GetPacketHandler(id)
	if err != nil {
		glog.Error(err)
		return nil, nil
	}

	glog.Info(fmt.Sprintf("removed PacketHandler %s", id))
	delete(a.PacketHandlers, id)
	return handler, nil
}

// RemoveProfileHandler is used to remove a ProfileHandler from the archive. Returns the removed ProfileHandler.
func (a *Archive) RemoveProfileHandler(id string) (ProfileHandler, error) {
	handler, err := a.GetProfileHandler(id)
	if err != nil {
		glog.Error(err)
		return nil, nil
	}

	glog.Info(fmt.Sprintf("removed ProfileHandler %s", id))
	delete(a.ProfileHandlers, id)
	return handler, nil
}

// RemoveProfile is used to remove a Profile from the archive. Returns the removed Profile.
func (a *Archive) RemoveProfile(id string) (*Profile, error) {
	profile, err := a.GetProfile(id)
	if err != nil {
		glog.Error(err)
		return nil, nil
	}

	defer a.UpdateVersion()

	glog.Info(fmt.Sprintf("removed profile %s: %s", profile.UUID, profile))
	a.RemovePacketHandler(id)
	delete(a.Profiles, id)
	return profile, nil
}

// RemoveRoom is used to remove a Room from the archive. Returns the removed Room.
func (a *Archive) RemoveRoom(id string) (*Room, error) {
	room, err := a.GetRoom(id)
	if err != nil {
		glog.Error(err)
		return nil, nil
	}

	defer a.UpdateVersion()

	glog.Info(fmt.Sprintf("removed room %s: %s", room.UUID, room))
	a.RemovePacketHandler(id)
	a.RemoveProfileHandler(id)
	delete(a.Rooms, id)
	return room, nil
}

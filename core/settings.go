package core

import (
	"crypto/md5"
	"encoding/json"
	"io/ioutil"
	"net"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/golang/glog"
)

//SystemSettings is the settings that relate to the program, not a server
var SystemSettings GeneralSettings

func init() {
	LoadSystemSettings()
}

//GeneralSettings is a struct that is used to store stuff that relates to the program at large.
type GeneralSettings struct {
	EnableMarkup       bool
	SoundNotifications bool
	SaveChatLogs       bool
	MuteVoice          bool // NOTE remove this after adding actual voice muting
	StylesheetPath     string
	FontPath           string
	OpenPassword       string
	FilesDirectory     string
	TimeExample        string
}

//LoadSystemSettings is used to load settings for the whole system/program
func LoadSystemSettings() {
	//If the folder doesn't exist, create it
	if _, err := os.Stat("profiles/"); os.IsNotExist(err) {
		os.Mkdir("profiles", os.ModePerm)
	}

	filepath := filepath.Join("./profiles/", systemSettingsFileName)
	fileContents, err := ioutil.ReadFile(filepath)

	if err != nil {
		SystemSettings.EnableMarkup = true
		SystemSettings.SoundNotifications = true
		SystemSettings.TimeExample = "11:30pm Jan _2"
	} else {
		err = json.Unmarshal([]byte(fileContents), &SystemSettings)

		if err != nil {
			glog.Error(err.Error())
		}
	}
}

//GenerateIPHash takes an IP and then returns a base36 encoded hash
func GenerateIPHash(servername string) string {
	index := strings.IndexRune(servername, ':')
	if index >= 0 {
		servername = servername[:index]
	}

	//This generates a hash with the ip, not domain
	ipNotDomain, err := net.LookupIP(servername)

	var profHash [16]byte

	if err != nil {
		profHash = md5.Sum([]byte(servername))
	} else {
		profHash = md5.Sum([]byte(ipNotDomain[0].String()))
	}

	for j, i := range profHash {
		profHash[j] = strconv.FormatUint(uint64(i), 36)[0]
	}

	return string(profHash[:len(profHash)])
}

//SaveSystemSettings is used to load settings for the whole system/program
func SaveSystemSettings() {
	filepath := filepath.Join("./profiles/", systemSettingsFileName)
	file, err := os.Create(filepath)
	if err != nil {
		glog.Error(err.Error())
	}

	bytes, err := json.Marshal(SystemSettings)
	if err != nil {
		glog.Error(err.Error())
	}
	file.Write(bytes)
}

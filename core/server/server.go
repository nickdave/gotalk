package server

import (
	"encoding/json"
	"errors"
	"fmt"
	"gotalk/core"
	"net"

	"github.com/golang/glog"
	"github.com/google/uuid"
)

// Server handles server-side logic
type Server struct {
	Archive core.Archive
	UUID    uuid.UUID
}

// NewServer creates a new server.
func NewServer() (s *Server) {
	id, _ := uuid.NewRandom()

	return &Server{*core.NewArchive(),
		id}
}

// Send sends the given packet directly to all clients on the server.
func (s *Server) Send(packet *core.Packet) error {
	if packet == nil {
		return errors.New("packet was nil")
	}

	for _, profile := range s.Archive.Profiles {
		profile.Send(packet)
	}

	return nil
}

// Receive evaluates the given packet and does something with it.
func (s *Server) Receive(packet *core.Packet) error {
	if packet == nil {
		return errors.New("packet was nil")
	}

	glog.Info(fmt.Sprintf("server processing a packet of type %s", packet.PacketType.String()))

	origProfile, err := s.Archive.GetProfile(packet.OrigUUID.String())
	if err != nil {
		glog.Error(err)
		return nil
	}

	switch packet.PacketType {
	case core.IDPacketDisconnection:
		s.Disconnect(origProfile)
	case core.IDPacketRoomCreation:
		var room *core.Room
		roomRecieved, err := core.NewRoom(&s.Archive, "", "", s)
		if err != nil {
			glog.Error(err)
			return nil
		}

		json.Unmarshal([]byte(packet.String()), roomRecieved)

		for _, room = range s.Archive.Rooms {
			if room == nil {
				continue
			}

			if roomRecieved.Name == room.Name {
				break
			}

			room = nil
		}

		if room == nil {
			room = roomRecieved
			s.Archive.AddRoom(room)
		}

		room.Connect(origProfile)

		s.SendArchive()

		welcome, _ := core.NewPacket(core.IDPacketChat, uuid.New(), origProfile.UUID, []byte(fmt.Sprintf("*_%s_ has connected to %s*", origProfile, room)))
		room.Send(welcome)
	case core.IDPacketChat:
	default:
		glog.Info("server sent a packet to handler")
		handler, err := s.Archive.GetPacketHandler(packet.DestUUID.String())
		if err != nil {
			profile, err := s.Archive.GetProfile(packet.ConnUUID.String())
			if err != nil {
				glog.Error(err)
				return nil
			}

			if profile.Client == nil {
				return nil
			}

			profile.Client.AddInputPacket(packet)
			return nil
		}

		handler.Receive(packet)
	}

	return nil
}

// Update handles the regular server updates. Needs to be called externally.
func (s *Server) Update() error {
	// Handles all the updates from all the client connections
	for _, profile := range s.Archive.Profiles {
		profile.Update()
	}

	return nil
}

// SendPacket creates a new Packet with the given data and sends it to all clients.
func (s *Server) SendPacket(t core.PacketType, data []byte) error {
	for _, profile := range s.Archive.Profiles {
		err0 := profile.SendPacket(t, profile.UUID, data)
		if err0 != nil {
			glog.Warning(err0)
			return nil
		}
	}

	return nil
}

// HandleNewClient handles new client connections.
func (s *Server) HandleNewClient(conn net.Conn) error {
	client, err := core.NewClient(conn, s, s)
	if err != nil {
		glog.Error(err)
		return nil
	}

	data, _ := client.GetData()
	packet, err := core.ReadPacket(data) // First packet is the client structure in JSON
	if err != nil {
		glog.Error(err)
		client.Close()
		return nil
	}

	if packet.Empty() {
		glog.Error("client JSON data is empty")
		return nil
	}

	profile := new(core.Profile)
	profile.SetClient(client)

	err = json.Unmarshal(packet.Data, profile)
	if err != nil {
		glog.Error(err)
		return nil
	}

	s.Connect(profile)

	glog.Info(fmt.Sprintf("%+v", *profile))

	glog.Info(fmt.Sprintf("%s connected to the server", profile))
	//s.SendPacket(IDPacketChat, []byte(client.Username+" has joined the server!"))

	return nil
}

// Close is used to shut down the server safely.
func (s *Server) Close() {
	s.SendPacket(core.IDPacketDisconnection, make([]byte, 0))

	for _, profile := range s.Archive.Profiles {
		profile.Close()
	}
}

// Connect adds a client profile to the server
func (s *Server) Connect(profile *core.Profile) error {
	if profile == nil {
		return errors.New("profile was nil")
	}

	err := s.Archive.AddProfile(profile)
	if err != nil {
		glog.Error(err)
		return nil
	}

	s.SendArchive()

	return nil
}

// Disconnect removes a client profile from the server
func (s *Server) Disconnect(profile *core.Profile) error {
	if profile == nil {
		return errors.New("profile was nil")
	}

	for _, room := range s.Archive.Rooms {
		room.Disconnect(profile)
	}

	glog.Info(fmt.Sprintf("%s disconnected from the server\n", profile))

	_, err := s.Archive.RemoveProfile(profile.UUID.String())
	if err != nil {
		glog.Warning(err)
	}

	profile.Close()

	s.SendArchive()

	return nil
}

// SendArchive sends an archive update to all clients.
func (s *Server) SendArchive() error {
	data, err := json.Marshal(s.Archive)
	if err != nil {
		glog.Error(err)
		return nil
	}

	s.SendPacket(core.IDPacketArchive, data)

	return nil
}

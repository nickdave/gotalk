package core

import (
	"errors"
	"fmt"

	"github.com/golang/glog"
	"github.com/google/uuid"
)

// MaxRooms is the max number of rooms on a server
const (
	MaxRooms = 65536
)

// NewRoom creates a new Room
func NewRoom(a *Archive, name string, pass string, handler PacketHandler) (*Room, error) {
	if handler == nil {
		return nil, errors.New("handler was nil")
	}

	id, _ := uuid.NewRandom()

	return &Room{name,
		pass,
		id,
		make(map[string]*Profile, MaxClients),
		a,
		handler}, nil
}

// Room is responsible for sending packets to groups of users.
type Room struct {
	Name     string
	Pass     string // TODO CHANGE THIS
	UUID     uuid.UUID
	Profiles map[string]*Profile
	archive  *Archive
	handler  PacketHandler
}

// String prints the room's name
func (r *Room) String() string {
	return r.Name
}

// Connect adds the client to the room.
func (r *Room) Connect(profile *Profile) error {
	if profile == nil {
		return errors.New("tried to connect a nil client to room")
	}

	glog.Info(fmt.Sprintf("%s connected to %s\n", profile, r))
	r.Profiles[profile.UUID.String()] = profile
	return nil
}

// Disconnect removes the client from the room.
func (r *Room) Disconnect(profile *Profile) error {
	if profile == nil {
		return errors.New("tried to disconnect a nil client from room")
	}

	glog.Info(fmt.Sprintf("%s disconnected from %s\n", profile, r))
	delete(r.Profiles, profile.UUID.String())
	return nil
}

// Send sends the given packet to all clients in the room.
func (r *Room) Send(packet *Packet) error {
	if packet == nil {
		return errors.New("packet was nil")
	}

	switch packet.PacketType {
	case IDPacketRoomChat:
		packet.PacketType = IDPacketChat
	case IDPacketRoomVoice:
		packet.PacketType = IDPacketVoice
	}
	packet.DestUUID = r.UUID // Send it to this room on the client

	for _, profile := range r.Profiles {
		if profile.UUID == packet.OrigUUID && packet.PacketType == IDPacketVoice {
			continue
		}

		packet.ConnUUID = profile.UUID // Update with our new connection info
		profile.Send(packet)
	}

	return nil
}

// Receive evaluates the given packet. Can either accept the packet itself or route it to the clients in the room.
func (r *Room) Receive(packet *Packet) error {
	if packet == nil {
		return errors.New("packet was nil")
	}

	glog.Info(fmt.Sprintf("room processing a packet of type %s", packet.PacketType.String()))

	switch packet.PacketType {
	case IDPacketRoomChat:
		r.Send(packet)
	case IDPacketRoomVoice:
		r.Send(packet)
	case IDPacketChat:
		if r.handler == nil {
			return errors.New("room's PacketHandler was nil")
		}
		r.handler.Receive(packet)
	case IDPacketConnection:
		profile, err := r.archive.GetProfile(packet.OrigUUID.String())
		if err != nil {
			glog.Error(err)
			break
		}
		r.Connect(profile)
	case IDPacketDisconnection:
		profile, err := r.archive.GetProfile(packet.OrigUUID.String())
		if err != nil {
			glog.Error(err)
			break
		}
		r.Disconnect(profile)
	default:
		glog.Info("room could not route packet")
	}

	return nil
}

// SetArchive sets the archive of the room.
func (r *Room) SetArchive(archive *Archive) error {
	r.archive = archive
	return nil
}

// SetHandler sets the PacketHandler of the room.
func (r *Room) SetHandler(handler PacketHandler) error {
	r.handler = handler
	return nil
}

package core

// PacketHandler handles a packet.
// Send is used to blindly send the given packet with no evaluation.
// Receive can evaluate the packet and send it somewhere specific.
type PacketHandler interface {
	Send(packet *Packet) error
	Receive(packet *Packet) error
}

// ProfileHandler handles a client.
// Connect is used to add the profile to a group (room/server).
// Disconnect is used to remove the profile from a group (room/server).
type ProfileHandler interface {
	Connect(profile *Profile) error
	Disconnect(profile *Profile) error
}

package ui

import (
	"gotalk/core"

	"github.com/google/uuid"
	"github.com/therecipe/qt/multimedia"
)

//Constants that relate to the quality of the recording
const (
	AudioChannelCount   = 1
	AudioSampleRate     = 16000
	AudioSampleSize     = 16
	AudioOpusFrameSize  = 40
	AudioOpusBitrate    = 77100
	AudioOpusBufferSize = 1000
)

//AudioFormat is the format that all VoIP uses. Can be changed later.
var AudioFormat *multimedia.QAudioFormat

//OutputDevice is the current output audio device in use
var OutputDevice *multimedia.QAudioDeviceInfo

//InputDevice is the current input device in use
var InputDevice *multimedia.QAudioDeviceInfo

//The list of sources of sound
var voiceInputs map[uuid.UUID]*VoiceOutput

//InputController is what reads the microphone
var InputController *VoiceInput

//InitializeVoIP is used to initialize various settings
func InitializeVoIP() {
	AudioFormat = multimedia.NewQAudioFormat()
	AudioFormat.SetSampleRate(AudioSampleRate)
	AudioFormat.SetChannelCount(AudioChannelCount)
	AudioFormat.SetSampleSize(AudioSampleSize)
	AudioFormat.SetCodec("audio/pcm")
	AudioFormat.SetByteOrder(multimedia.QAudioFormat__LittleEndian)
	AudioFormat.SetSampleType(multimedia.QAudioFormat__SignedInt)

	OutputDevice = multimedia.QAudioDeviceInfo_DefaultOutputDevice()
	InputDevice = multimedia.QAudioDeviceInfo_DefaultInputDevice()

	voiceInputs = make(map[uuid.UUID]*VoiceOutput)
	InputController = NewVoiceInput()
}

//GetPacket is whenever an audio packet is received. It adds it to the corresponding hash in the map.
func GetPacket(packet *core.Packet) {
	if voiceInputs[packet.OrigUUID] == nil {
		voiceInputs[packet.OrigUUID] = NewVoiceOutput()
		voiceInputs[packet.OrigUUID].GivePacket(packet)
	} else {
		voiceInputs[packet.OrigUUID].GivePacket(packet)
	}
}

//PlayPendingAudio is used to write pending audio packets to the output device.
func PlayPendingAudio() {
	for _, i := range voiceInputs {
		i.playAudio()
	}
}

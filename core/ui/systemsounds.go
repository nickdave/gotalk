package ui

import (
	"gotalk/core"
	"path/filepath"

	"github.com/golang/glog"
	qt "github.com/therecipe/qt/core"
	"github.com/therecipe/qt/multimedia"
)

/*
**The paths that give relative paths to sound files.
**THESE SHOULD BE CONCATENATED TO THE CHOSEN FILES PATH
 */
const (
	SoundBufferSize = 16

	SoundUserJoin    = "sounds/joining_room.wav"
	SoundChatMessage = "sounds/chat_message.wav"
)

//SoundPath is simply the constant relating to the sound path
type SoundPath string

var soundQueue chan string

func init() {
	soundQueue = make(chan string, SoundBufferSize)
}

//AddSound is used to add a sound to the sound queue
func AddSound(sound SoundPath) {
	if !core.SystemSettings.SoundNotifications {
		return
	}

	file := filepath.Join(core.SystemSettings.FilesDirectory, string(sound))
	filePath, err := filepath.Abs(file)
	if err != nil {
		glog.Error(err)
	}

	select {
	case soundQueue <- filePath:
	default:
		glog.Info("Sound stack full!")
	}

}

//PlaySound actually executes a sound based on the path
func PlaySound(absPath string) {
	player := multimedia.NewQMediaPlayer(app, 0)
	filePath := qt.NewQUrl().FromLocalFile(absPath)
	playlist := multimedia.NewQMediaPlaylist(player)
	playlist.AddMedia(multimedia.NewQMediaContent2(filePath))

	player.SetPlaylist(playlist)
	player.Play()
}

package ui

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"gotalk/core"
	"net"
	"os"

	"github.com/golang/glog"
	qt "github.com/therecipe/qt/core"
	"github.com/therecipe/qt/gui"
	"github.com/therecipe/qt/widgets"
)

var allUserList *widgets.QListWidget
var allRoomList *widgets.QListWidget
var roomUserList *widgets.QListWidget
var window *widgets.QMainWindow
var app *widgets.QApplication

var user *User

var ipFlag string
var usernameFlag string

func usage() {
	// TODO reimplement commandline ip and username input
	fmt.Fprintf(os.Stderr, "usage: gotalk -ip=127.0.0.1:9152 -username=johnsmith\n")
	flag.PrintDefaults()
	os.Exit(2)
}

func init() {
	user = NewUser()

	flag.Usage = usage

	flag.StringVar(&ipFlag, "ip", "127.0.0.1:9152", "IP for requested server")
	flag.StringVar(&usernameFlag, "username", "", "Username for the client")
	flag.Parse()
}

func configureMenuBar() {
	connectionMenu := widgets.NewQMenu2("&Connection", nil)
	disconnectAction := widgets.NewQAction2("&Disconnect", nil)
	reconnectAction := widgets.NewQAction2("C&onnect", nil)

	connectActions := []*widgets.QAction{reconnectAction, disconnectAction}
	connectionMenu.AddActions(connectActions)
	window.MenuBar().AddMenu(connectionMenu)

	settingsMenu := widgets.NewQMenu2("&Options", nil)
	licenseAction := widgets.NewQAction2("&License", nil)
	optionsAction := widgets.NewQAction2("&Options", nil)

	settingsActions := []*widgets.QAction{optionsAction, licenseAction}
	settingsMenu.AddActions(settingsActions)
	window.MenuBar().AddMenu(settingsMenu)

	roomMenu := widgets.NewQMenu2("&Rooms", nil)
	connectAction := widgets.NewQAction2("&New Room", nil)

	roomActions := []*widgets.QAction{connectAction}
	roomMenu.AddActions(roomActions)
	window.MenuBar().AddMenu(roomMenu)

	// Handle for if a piece on the menu bar is activated
	window.MenuBar().ConnectTriggered(func(action *widgets.QAction) {
		// A simple switch for determining which it was
		switch action.Text() {
		case disconnectAction.Text():
			// If they're not connected, don't try and close it.
			if user.profile != nil {
				user.Disconnect(user.profile)
			}
		case reconnectAction.Text():
			getIPInfo()
		case licenseAction.Text():
			ShowError("This project is created by David Bittner and Nick Hirzel. It is licensed under the GNU GPL v3 license, more info can be found at our gitlab page.")
		case connectAction.Text():
			getRoomInfo()
		case optionsAction.Text():
			showOptionsMenu()
		}
	})
}

// InitializeUI initilizes the Qt interface.
func InitializeUI() {
	// Length of zero because we do not want them to interfere
	app = widgets.NewQApplication(0, os.Args)
	window = widgets.NewQMainWindow(nil, 0)

	app.SetStyleSheet(LoadStyleSheet(core.SystemSettings.StylesheetPath))

	// Set basic info about the window
	window.SetWindowTitle("GoTalk")
	window.SetMinimumSize2(800, 600)

	// The area that encompasses all three sections
	overallArea := widgets.NewQHBoxLayout()

	// The three seperate areas
	messageArea := widgets.NewQVBoxLayout()
	leftListArea := widgets.NewQVBoxLayout()
	rightListArea := widgets.NewQVBoxLayout()

	roomTabPane = widgets.NewQTabWidget(nil)
	messageArea.AddWidget(roomTabPane, 0, 0)

	roomTabPane.ConnectTabBarClicked(func(index int) {
		currentTab = roomTabs[index]
	})

	// The left sections tabs/widgets
	leftTabArea := widgets.NewQTabWidget(nil)
	leftTabArea.SetMaximumWidth(150)
	allUserList = widgets.NewQListWidget(nil)
	allRoomList = widgets.NewQListWidget(nil)
	leftTabArea.AddTab(allUserList, "Users")
	leftTabArea.AddTab(allRoomList, "Rooms")
	leftListArea.AddWidget(leftTabArea, 0, 0)

	// The list for the individual rooms user list
	roomUserList := widgets.NewQListWidget(nil)
	rightListArea.AddWidget(roomUserList, 0, 0)
	roomUserList.SetMaximumWidth(150)

	// Add all three sections to the overall area
	overallArea.AddLayout(leftListArea, 0)
	overallArea.AddLayout(messageArea, 1)
	overallArea.AddLayout(rightListArea, 0)

	centralWidget := widgets.NewQWidget(window, 0)

	centralWidget.SetLayout(overallArea)
	window.SetCentralWidget(centralWidget)

	window.ConnectCloseEvent(func(event *gui.QCloseEvent) {
		app.Quit()
	})
	app.ConnectAboutToQuit(func() {
		if user == nil {
			return
		}

		if user.profile != nil {
			user.profile.Save(core.CurrentHash)
		}

		user.Close()

		core.SaveSystemSettings()
	})

	configureMenuBar()
	InitializeVoIP()
	window.Show()

	getIPInfo()
}

// InitializeNet initiliazes the network connection between the client and the server.
func InitializeNet(profile *core.Profile, ip string) {
	connection, err := net.Dial("tcp", ip)
	if err != nil {
		ShowError(err.Error())
		return
	}

	if connection == nil {
		ShowError("Failed to create connection instance.")
		return
	}

	if profile == nil {
		ShowError("User profile could not be loaded.")
		return
	}

	client, err := core.NewClient(connection, user, user)
	if err != nil {
		glog.Error(err)
		glog.Flush()
		return
	}

	profile.SetClient(client)
	user.SetProfile(profile)

	data, err := json.Marshal(user.profile)
	user.profile.SendPacket(
		core.IDPacketConnection,
		user.profile.UUID,
		[]byte(data)) // Sending profile data to server

	// This handles reading from the network and other regular updates
	updateTimer := qt.NewQTimer(window)
	updateTimer.StartTimer(5, 0)

	updateTimer.ConnectTimerEvent(func(event *qt.QTimerEvent) {
		user.Update()
		InputController.CollectData()
		InputController.SendData()
		PlayPendingAudio()
	})
}

func addToRoomList(room *core.Room) error {
	if allRoomList == nil {
		return errors.New("allRoomList not initialized")
	}

	if room == nil {
		return errors.New("room is nil")
	}

	allRoomList.AddItem(room.Name)

	return nil
}

func addToUserList(profile *core.Profile) error {
	if allUserList == nil {
		return errors.New("allUserList not initialized")
	}

	if profile == nil {
		return errors.New("profile is nil")
	}

	allUserList.AddItem(profile.Username)

	return nil
}

func removeFromUserList(profile *core.Profile) error {
	if allUserList == nil {
		return errors.New("allUserList not initialized")
	}

	if profile == nil {
		return errors.New("profile is nil")
	}

	// TODO this sucks, i hope there's a better way somewhere
	items := allUserList.FindItems(profile.Username, qt.Qt__MatchExactly)
	if items == nil || len(items) <= 0 {
		return errors.New("item does not exist")
	}

	allUserList.RemoveItemWidget(items[0])

	return nil
}

func removeFromRoomList(room *core.Room) error {
	if allRoomList == nil {
		return errors.New("allRoomList not initialized")
	}

	if room == nil {
		return errors.New("room is nil")
	}

	// TODO this also sucks
	items := allRoomList.FindItems(room.Name, qt.Qt__MatchExactly)
	if items == nil || len(items) <= 0 {
		return errors.New("item does not exist")
	}

	allRoomList.RemoveItemWidget(items[0])

	return nil
}

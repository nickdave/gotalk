package ui

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"gotalk/core"

	"github.com/golang/glog"
	"github.com/hraban/opus"
	qt "github.com/therecipe/qt/core"
	"github.com/therecipe/qt/multimedia"
)

//VoiceInput is used to retrieve input from the microphone
type VoiceInput struct {
	input  *multimedia.QAudioInput
	device *qt.QIODevice

	data        *bytes.Buffer
	encodedData *bytes.Buffer
	encoder     *opus.Encoder

	chunkSize int
}

//NewVoiceInput is a 'constructor'
func NewVoiceInput() (vi *VoiceInput) {
	var err error

	vi = new(VoiceInput)
	vi.data = bytes.NewBuffer(nil)
	vi.encodedData = bytes.NewBuffer(nil)
	vi.encoder, err = opus.NewEncoder(AudioSampleRate, AudioChannelCount, opus.AppVoIP)
	if err != nil {
		glog.Error(err.Error())
	}
	vi.encoder.SetBitrateAuto()
	vi.encoder.SetMaxBandwidth(opus.Narrowband)

	vi.input = multimedia.NewQAudioInput2(InputDevice, AudioFormat, nil)
	vi.device = vi.input.Start2()
	vi.device.ConnectReadyRead(vi.readData)

	if err != nil {
		glog.Error(err.Error())
	}

	return
}

func (vi *VoiceInput) readData() {
	if currentTab == nil || core.SystemSettings.MuteVoice {
		vi.device.ReadAll()
		return
	}

	data := new(string)
	vi.device.ReadData(data, 10000)
	vi.data.Write([]byte(*data))
}

func convertInt16(data []byte) int16 {
	if len(data) > 2 {
		fmt.Println("Too many bytes you dingus")
		return -1
	}
	tempVar := binary.LittleEndian.Uint16(data)
	outData := (int16)(tempVar)
	return outData
}

func byteToInt16(data []byte) []int16 {
	size := 2
	ret := make([]int16, len(data)/size)
	for i := 0; i < len(ret); i++ {
		ret[i] = convertInt16(data[i*size : (i+1)*size])
	}
	return ret
}

func uint32toBytes(in uint32) []byte {
	holder := make([]byte, 4)
	binary.LittleEndian.PutUint32(holder, in)
	return holder
}

/*
Voice Packet structure:
A 32 bit integer stating the frame length immediately followed by that frame's data.
It can be assumed that the data is in the proper format.
*/

//CollectData is used to send recorded data. It includes the encoding process.
func (vi *VoiceInput) CollectData() {
	if vi == nil {
		return
	}

	vi.chunkSize = 0
	pullCount := ((AudioOpusFrameSize * AudioSampleRate) / 1000 * AudioChannelCount)

	for vi.data.Len() >= pullCount {
		pulled := make([]byte, pullCount)
		encoded := make([]byte, AudioOpusBufferSize)
		vi.data.Read(pulled)

		n, err := vi.encoder.Encode(byteToInt16(pulled), encoded)
		if err != nil {
			glog.Error(err.Error())
		}

		encoded = encoded[:n]
		vi.encodedData.Write(uint32toBytes(uint32(len(encoded))))
		vi.encodedData.Write(encoded)

		convertedLengthSize := 4
		vi.chunkSize += convertedLengthSize + len(encoded)
	}

}

//SendData is used to actually package up the data, and send it.
func (vi *VoiceInput) SendData() {
	//If there is enough or more than enough data to fill a packet:
	if vi.encodedData.Len() >= core.MaxPacketBodySize {
		useLength := vi.encodedData.Len()
		if useLength > core.MaxPacketBodySize {
			useLength -= vi.chunkSize
		}

		data := make([]byte, useLength)
		vi.encodedData.Read(data)
		user.profile.SendPacket(core.IDPacketRoomVoice, currentTab.room.UUID, data)
	}
}

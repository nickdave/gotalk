package ui

import (
	"encoding/json"
	"errors"
	"fmt"
	"gotalk/core"

	"github.com/golang/glog"
	"github.com/google/uuid"
)

// User recieves and sends data to the interface.
type User struct {
	Archive   *core.Archive
	UUID      uuid.UUID
	profile   *core.Profile // The current active client
	roomQueue chan *core.Room
}

// NewUser creates a new user.
func NewUser() (u *User) {
	id, _ := uuid.NewRandom()

	return &User{core.NewArchive(),
		id,
		nil,
		make(chan *core.Room, core.MaxRooms)}
}

// Send sends the given packet to the destination.
func (u *User) Send(packet *core.Packet) error {
	if packet == nil {
		return errors.New("packet was nil")
	}

	handler, err := u.Archive.GetPacketHandler(packet.DestUUID.String())
	if err != nil {
		profile, err := u.Archive.GetProfile(packet.ConnUUID.String())
		if err != nil {
			glog.Error(err)
			return nil
		}

		profile.AddInputPacket(packet)
		return nil
	}

	glog.Info("user sending packet to a PacketHandler")
	handler.Receive(packet)

	return nil
}

// Receive evaluates the given packet and does something with it.
func (u *User) Receive(packet *core.Packet) error {
	if packet == nil {
		return errors.New("given packet was nil")
	}

	glog.Info(fmt.Sprintf("user processing a packet of type %s", packet.PacketType.String()))
	switch packet.PacketType {
	case core.IDPacketDisconnection:
		profile, err := u.Archive.GetProfile(packet.OrigUUID.String())
		if err != nil {
			glog.Error(err)
			return nil
		}

		u.Disconnect(profile)
	case core.IDPacketArchive:
		glog.Info("archive update recieved")

		archive := core.NewArchive()
		err := json.Unmarshal(packet.Data, archive)
		if err != nil {
			glog.Error(err)
			return nil
		}

		err = u.UpdateArchive(archive)
		if err != nil {
			glog.Error(err)
			return nil
		}
	default:
		u.Send(packet)
	}

	return nil
}

// Update handles the regular user updates. Needs to be called externally.
func (u *User) Update() error {
	// Reads the data from the network
	for _, profile := range u.Archive.Profiles {
		profile.Update()
	}

	select {
	case room := <-u.roomQueue:
		glog.Info("Adding new RoomTab")
		AddSound(SoundUserJoin)
		addRoomTab(room)
	case soundPath := <-soundQueue:
		PlaySound(string(soundPath))
	default:
	}

	return nil
}

// Close closes the user safely.
func (u *User) Close() {
	u.Disconnect(u.profile)
}

// SetProfile sets the user profile to the given profile.
func (u *User) SetProfile(profile *core.Profile) error {
	if profile == nil {
		return errors.New("profile was nil")
	}

	err := u.Connect(profile)
	if err != nil {
		glog.Warning(err)
	}

	u.profile = profile

	return nil
}

// Connect adds a profile to the user
func (u *User) Connect(profile *core.Profile) error {
	if profile == nil {
		return errors.New("profile was nil")
	}

	err := u.Archive.AddProfile(profile)
	addToUserList(profile)

	if err != nil {
		glog.Warning(err)
	}

	return nil
}

// Disconnect removes a profile from the user
func (u *User) Disconnect(profile *core.Profile) error {
	if profile == nil {
		return errors.New("profile was nil")
	}

	glog.Info(fmt.Sprintf("%s disconnected from the server", profile))

	if u.profile == profile {
		u.profile.Save(core.CurrentHash)
		profile.Close()

		user.profile = nil
		core.CurrentHash = ""

		roomTabPane.Clear()

		allUserList.Clear()
		allRoomList.Clear()

		u.Archive = core.NewArchive()
		return nil
	}

	removeFromUserList(profile)

	for _, room := range u.Archive.Rooms {
		room.Disconnect(profile)
	}

	_, err := u.Archive.RemoveProfile(profile.UUID.String())
	if err != nil {
		glog.Warning(err)
	}

	profile.Close()

	return nil
}

// UpdateArchive updates the original archive with the given.
func (u *User) UpdateArchive(given *core.Archive) error {
	if given == nil {
		return errors.New("archive is nil")
	}

	a := u.Archive

	// Removes nonexistant Profiles
	for _, profile := range a.Profiles {
		_, err := given.GetProfile(profile.UUID.String())
		if err != nil {
			u.Disconnect(profile)
		}
	}

	// Adds new Profiles
	for _, profile := range given.Profiles {
		_, err := a.GetProfile(profile.UUID.String())
		if err != nil {
			u.Connect(profile)
		}
	}

	// TODO Updates existing Profiles if usernames / descriptions change

	// Removes nonexistant Rooms
	for _, room := range a.Rooms {
		_, err := given.GetRoom(room.UUID.String())
		if err != nil {
			a.RemoveRoom(room.UUID.String())
			removeFromRoomList(room)
		}
	}

	// Adds new Rooms
	for _, room := range given.Rooms {
		origRoom, err := a.GetRoom(room.UUID.String())
		if err != nil {
			room.SetArchive(a) // Sets the room archive to the user archive

			for id := range room.Profiles {
				profile, err := a.GetProfile(id)
				if err != nil {
					continue
				}

				glog.Info(fmt.Sprintf("archive-u: setting profile in %s to existing profile %s", room, profile))
				room.Profiles[id] = profile

				if profile == u.profile {
					u.roomQueue <- room
				}
			}

			a.AddRoom(room)
			addToRoomList(room)
		} else {
			// update the existing room
			// Remove disconnected profiles
			for id := range origRoom.Profiles {
				if room.Profiles[id] != nil {
					continue
				}

				profile, err := a.GetProfile(id)
				if err != nil {
					continue
				}
				glog.Info(fmt.Sprintf("archive-u: disconnecting profile %s from %s", profile, room))
				origRoom.Disconnect(profile)
			}

			// Add connected profiles
			for id := range room.Profiles {
				if origRoom.Profiles[id] != nil {
					continue
				}

				profile, err := a.GetProfile(id)
				if err != nil {
					continue
				}
				glog.Info(fmt.Sprintf("archive-u: connecting profile %s to %s", profile, room))
				origRoom.Connect(profile)
				if profile == u.profile {
					u.roomQueue <- origRoom
				}
			}
		}
	}

	// Updates existing Rooms

	return nil
}

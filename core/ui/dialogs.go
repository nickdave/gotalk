package ui

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"gotalk/core"
	"reflect"
	"strings"

	"github.com/golang/glog"
	"github.com/google/uuid"
	qt "github.com/therecipe/qt/core"
	"github.com/therecipe/qt/gui"
	"github.com/therecipe/qt/widgets"
)

//ShowError for basic errors, something that doesn't require action.
func ShowError(msg string) {
	er := widgets.NewQMessageBox(nil)
	er.SetText(msg)
	er.Show()
}

func getIPInfo() {
	ip := "Server IP"
	servPass := "Server Pass"

	initNet := func() {
		//The dialog to get the info
		if user.profile != nil {
			user.Disconnect(user.profile)
		}

		core.CurrentHash = core.GenerateIPHash(ip)

		profile, err := core.LoadProfile(core.CurrentHash)
		if err != nil {
			showUserDialog(ip)
		} else if profile != nil {
			if !strings.Contains(ip, ":9152") {
				ip = ip + ":9152"
			}

			InitializeNet(profile, ip)
		}
	}

	twoInputDialog(&ip, &servPass, initNet)
}

func getRoomInfo() {
	if user.profile == nil {
		ShowError("Not currently connected to a server!")
		return
	}

	pass := "Room Password"
	name := "Room Name"

	connectRoom := func() {
		room, err := core.NewRoom(user.Archive, name, pass, user)
		if err != nil {
			ShowError(fmt.Sprintf("An error occured while creating the room:\n %s", err.Error()))
			glog.Error(err)
			return
		}

		data, err := json.Marshal(room)
		if err != nil {
			ShowError(fmt.Sprintf("An error occured while creating the room:\n %s", err.Error()))
			glog.Error(err)
			return
		}

		user.profile.SendPacket(core.IDPacketRoomCreation, user.profile.UUID, data)
		glog.Info("room creation packet sent")
	}

	twoInputDialog(&name, &pass, connectRoom)
}

func twoInputDialog(field1, field2 *string, do func()) {
	//Create the window for the dialog
	dialog := widgets.NewQDialog(nil, 0)
	layout := widgets.NewQVBoxLayout()
	dialog.SetLayout(layout)

	//Create all the seperate boxes for initializing the Net connections
	abox := widgets.NewQLineEdit(nil)
	abox.SetPlaceholderText(*field1)
	bbox := widgets.NewQLineEdit(nil)
	bbox.SetPlaceholderText(*field2)
	okButton := widgets.NewQPushButton(nil)
	okButton.SetText("Done")

	//Add the seperate widgets
	layout.AddWidget(abox, 1, qt.Qt__AlignCenter)
	layout.AddWidget(bbox, 1, qt.Qt__AlignCenter)
	layout.AddWidget(okButton, 1, qt.Qt__AlignCenter)

	dialog.SetFixedSize(dialog.SizeHint())

	dialog.Show()

	okButton.ConnectPressed(func() {
		*field1 = abox.Text()
		*field2 = bbox.Text()

		dialog.Close()

		do()
	})
}

func showUserDialog(ip string) {
	dialog := widgets.NewQDialog(window, 0)
	profile := new(core.Profile)
	profile.UUID, _ = uuid.NewRandom()

	layout, err := createUserLayout(profile, dialog)
	if err != nil {
		ShowError(err.Error())
		return
	}

	dialog.SetLayout(layout)
	dialog.SetMaximumHeight(dialog.MinimumHeight())
	dialog.Show()

	dialog.ConnectCloseEvent(func(event *gui.QCloseEvent) {
		InitializeNet(profile, ip)
	})
}

func showOptionsMenu() {
	dialog := widgets.NewQDialog(window, 0)
	mainLayout := widgets.NewQVBoxLayout()

	generalTab := widgets.NewQWidget(nil, 0)
	generalTab.SetLayout(generateDialog(&core.SystemSettings))

	userTab := widgets.NewQWidget(nil, 0)
	layout, err := createUserLayout(user.profile, nil)
	if err != nil {
		ShowError(err.Error())
		return
	}

	userTab.SetLayout(layout)

	tabWidget := widgets.NewQTabWidget(dialog)
	tabWidget.AddTab(generalTab, "General Settings")
	tabWidget.AddTab(userTab, "User Settings")

	mainLayout.AddWidget(tabWidget, 1, 0)
	dialog.SetLayout(mainLayout)
	dialog.Show()
}

func isUpper(byt byte) bool {
	return (byt >= 65 && byt <= 90)
}

func formatName(varname string) string {
	var buffer bytes.Buffer
	for i := 0; i < len(varname)-1; i++ {
		buffer.Write([]byte{varname[i]})
		if (isUpper(varname[i]) != isUpper(varname[i+1])) && !isUpper(varname[i]) {
			buffer.Write([]byte{' '})
		}
	}
	buffer.Write([]byte{varname[len(varname)-1]})
	return buffer.String()
}

//The dialog for creating a new user
func createUserLayout(profile *core.Profile, parent *widgets.QDialog) (*widgets.QVBoxLayout, error) {
	if profile == nil {
		return nil, errors.New("profile was nil")
	}

	layout := widgets.NewQVBoxLayout()

	fileLayout := widgets.NewQGroupBox2("Avatar", nil)
	hFileLayout := widgets.NewQHBoxLayout()
	filePathLabel := widgets.NewQLabel2("File Path", nil, 0)
	dialogButton := widgets.NewQPushButton2("...", nil)
	hFileLayout.AddWidget(filePathLabel, 1, qt.Qt__AlignLeft)
	hFileLayout.AddWidget(dialogButton, 1, qt.Qt__AlignRight)
	fileLayout.SetLayout(hFileLayout)

	fileLayout.SetMaximumHeight(filePathLabel.Height())

	layout.AddWidget(fileLayout, 0, 0)

	userInfoLayout := widgets.NewQGroupBox2("User Information", nil)
	vUserInfoLayout := widgets.NewQVBoxLayout()
	userInfoLayout.SetLayout(vUserInfoLayout)
	usernameBox := widgets.NewQLineEdit(nil)
	usernameBox.SetPlaceholderText("Username")
	locationBox := widgets.NewQLineEdit(nil)
	locationBox.SetPlaceholderText("Location (optional)")
	descriptionBox := widgets.NewQTextEdit(nil)
	descriptionBox.SetPlaceholderText("Description (optional)")

	vUserInfoLayout.AddWidget(usernameBox, 1, 0)
	vUserInfoLayout.AddWidget(locationBox, 1, 0)
	vUserInfoLayout.AddWidget(descriptionBox, 1, 0)

	layout.AddWidget(userInfoLayout, 1, 0)

	finishedButton := widgets.NewQPushButton2("Done", nil)
	layout.AddWidget(finishedButton, 1, 0)

	usernameBox.SetText(profile.Username)
	locationBox.SetText(profile.Location)
	filePathLabel.SetText("Avatar: " + profile.Avatar)
	descriptionBox.Append(profile.Description)

	dialogButton.ConnectPressed(func() {
		fileDialog := widgets.NewQFileDialog(nil, 0)
		fileDialog.Show()
		fileDialog.ConnectFileSelected(func(file string) {
			if core.CurrentHash != "" {
				profile.SetAvatar(file)
				filePathLabel.SetText("Avatar: " + file)
			} else {
				ShowError("You are not connected to a server!")
			}
		})
	})

	finishedButton.ConnectPressed(func() {
		if len(usernameBox.Text()) < core.MinUsernameSize || len(usernameBox.Text()) > core.MaxUsernameSize {
			ShowError(fmt.Sprintf("You must enter a username between %d and %d characters long.", core.MinUsernameSize, core.MaxUsernameSize))
		} else if core.CurrentHash != "" {
			profile.SetUsername(usernameBox.Text())
			profile.SetDescription(descriptionBox.ToPlainText())
			profile.SetLocation(locationBox.Text())

			if parent != nil {
				parent.Close()
			} else {
				profile.Save(core.CurrentHash)
			}
		} else {
			ShowError("You aren't connected to a server!")
		}
	})

	return layout, nil
}

//Used to automatically generate a dialog based on the contents of a struct
func generateDialog(in interface{}) *widgets.QFormLayout {
	vars := reflect.ValueOf(in)
	info := reflect.Indirect(vars)

	layout := widgets.NewQFormLayout(nil)

	//Iterate through each member of the struct and see if it can be displayed
	for i := 0; i < vars.Elem().NumField(); i++ {
		switch vars.Elem().Field(i).Interface().(type) {
		case bool:
			tempCheck := widgets.NewQCheckBox(nil)
			tempCheck.SetChecked(vars.Elem().Field(i).Bool())
			layout.AddRow3(formatName(info.Type().Field(i).Name), tempCheck)

			id := i
			tempCheck.ConnectClicked(func(checked bool) {
				vars.Elem().Field(id).SetBool(checked)
			})
		case string:
			id := i

			//If it the name has path or directory, it's a file dialog.
			//If it's either, then that just place a standard string entry box
			if strings.Contains(info.Type().Field(i).Name, "Path") || strings.Contains(info.Type().Field(i).Name, "Directory") {
				holderWidget := widgets.NewQWidget(nil, 0)
				horiLayout := widgets.NewQHBoxLayout()
				pathLabel := widgets.NewQLabel2(vars.Elem().Field(i).String(), nil, 0)
				dialogButton := widgets.NewQPushButton2("...", nil)

				horiLayout.AddWidget(pathLabel, 1, qt.Qt__AlignTop)
				horiLayout.AddWidget(dialogButton, 0, qt.Qt__AlignTop)
				holderWidget.SetLayout(horiLayout)

				dialogButton.ConnectPressed(func() {
					dialog := widgets.NewQFileDialog(nil, 0)
					if strings.Contains(info.Type().Field(id).Name, "Directory") {
						dialog.SetOption(widgets.QFileDialog__ShowDirsOnly, true)
						dialog.SetFileMode(widgets.QFileDialog__DirectoryOnly)
						fmt.Println("here")
					}
					dialog.Show()
					dialog.ConnectFileSelected(func(file string) {
						vars.Elem().Field(id).SetString(file)
						pathLabel.SetText(vars.Elem().Field(id).String())
					})
				})

				layout.AddRow3(formatName(info.Type().Field(i).Name), holderWidget)
			} else {
				lineEdit := widgets.NewQLineEdit(nil)

				//If it is a password, hide the text
				if strings.Contains(info.Type().Field(i).Name, "Password") {
					lineEdit.SetEchoMode(widgets.QLineEdit__Password)
					lineEdit.SetInputMethodHints(qt.Qt__ImhHiddenText | qt.Qt__ImhNoAutoUppercase)
				}

				lineEdit.ConnectTextChanged(func(text string) {
					vars.Elem().Field(id).SetString(text)
				})

				layout.AddRow3(formatName(info.Type().Field(i).Name), lineEdit)
			}
		default:
			continue
		}
	}

	return layout
}

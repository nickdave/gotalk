package ui

import (
	"fmt"
	"io/ioutil"
)

// LoadStyleSheet is for loading a style sheet from a file and returning it as a single string.
func LoadStyleSheet(filepath string) (retString string) {
	data, err := ioutil.ReadFile(filepath)
	if err != nil {
		fmt.Println(err.Error())
	}

	for _, byt := range data {
		retString += string(byt)
	}

	return retString
}

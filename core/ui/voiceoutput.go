package ui

import (
	"bytes"
	"encoding/binary"
	"gotalk/core"
	"sync"

	"github.com/golang/glog"
	"github.com/hraban/opus"
	qt "github.com/therecipe/qt/core"
	"github.com/therecipe/qt/multimedia"
)

//VoiceOutput is what handles a specific audio device for writing data.
//It decodes received Opus data
type VoiceOutput struct {
	packets chan *core.Packet

	playBuffer  *bytes.Buffer
	opusBuffer  *bytes.Buffer
	mutex       *sync.Mutex
	opusDecoder *opus.Decoder

	output       *multimedia.QAudioOutput
	outputDevice *qt.QIODevice
}

//NewVoiceOutput is a 'constructor'
func NewVoiceOutput() (vo *VoiceOutput) {

	var err error

	vo = new(VoiceOutput)
	vo.output = multimedia.NewQAudioOutput2(OutputDevice, AudioFormat, nil)
	vo.outputDevice = vo.output.Start2()

	vo.mutex = &sync.Mutex{}

	vo.opusBuffer = bytes.NewBuffer(nil)
	vo.playBuffer = bytes.NewBuffer(nil)
	vo.opusDecoder, err = opus.NewDecoder(AudioSampleRate, AudioChannelCount)

	if err != nil {
		glog.Error(err.Error())
	}

	//This channel is where all of the pending packets are stored.
	vo.packets = make(chan *core.Packet, 32)

	return vo

}

//GivePacket is to add a packet to the packet stack
func (vo *VoiceOutput) GivePacket(packet *core.Packet) {
	vo.packets <- packet
}

//This is a weird function. Converts an integer array, to a byte array, to a string. Needed for Qt writing.
//C++ uses const char* for this data, so what's that in go? A string of course!
func intToByteToString(in []int16) string {
	outData := make([]byte, len(in)*2)
	holder := make([]byte, 2)

	//Iterate through each value and convert it to bytes
	for count, i := range in {
		binary.LittleEndian.PutUint16(holder, uint16(i))

		outData[count*2] = holder[0]
		outData[(count*2)+1] = holder[1]
	}

	return string(outData)
}

func (vo *VoiceOutput) playAudio() {
	pullCount := ((AudioOpusFrameSize * AudioSampleRate) / 1000 * AudioChannelCount)

	//If a packet gets received within half the frame time of an audio chunk add it to the buffer
	//If not, play pending audio
	select {
	case dat := <-vo.packets:
		vo.playBuffer.Write(dat.Data)

	default:
		if vo.playBuffer.Len() > 0 {
			//The length (in bytes) of the frame about to be decoded.
			frameLen := make([]byte, 4)
			vo.playBuffer.Read(frameLen)
			length := binary.LittleEndian.Uint32(frameLen)
			if length == 0 {
				return
			}

			//Create an array of data to be read that decodes.
			readDat := make([]byte, length)
			vo.playBuffer.Read(readDat)

			//PCM contains the decoded data.
			pcm := make([]int16, pullCount)
			n, err := vo.opusDecoder.Decode(readDat, pcm)

			//Clip off any excess data.
			pcm = pcm[:n*AudioChannelCount]

			if err != nil {
				glog.Error(err.Error())
				return
			}
			temp := intToByteToString(pcm)
			vo.outputDevice.WriteData(temp, int64(len(temp)))
		}
	}
}

package ui

import (
	"bytes"
	"errors"
	"fmt"
	"gotalk/core"
	"time"

	qt "github.com/therecipe/qt/core"
	"github.com/therecipe/qt/gui"
	"github.com/therecipe/qt/widgets"
)

const maxRooms = 255

// The pane in the center that contains the room tab.
var roomTabPane *widgets.QTabWidget

var currentTab *RoomTab
var roomTabs []*RoomTab

// RoomTab holds the Room object on the client side and is the representation of it on the user-interface.
type RoomTab struct {
	holder     *widgets.QVBoxLayout
	messageBox *widgets.QTextBrowser
	enterBox   *widgets.QLineEdit
	room       *core.Room
}

func init() {
	roomTabs = make([]*RoomTab, 0)
}

// NewRoomTab creates a new RoomTab.
func NewRoomTab(room *core.Room) (rt *RoomTab) {
	// This creates the new message box and entry bar for each tab created.
	rt = new(RoomTab)
	rt.room = room

	rt.enterBox = widgets.NewQLineEdit(nil)
	rt.messageBox = widgets.NewQTextBrowser(nil)
	rt.messageBox.SetReadOnly(true)

	// Sets up the layout by adding in the various pieces
	rt.holder = widgets.NewQVBoxLayout()
	rt.holder.SetSpacing(5)
	rt.holder.SetContentsMargins(1, 1, 1, 1)
	rt.holder.AddWidget(rt.messageBox, 0, 0)
	rt.holder.AddWidget(rt.enterBox, 0, 0)

	rt.enterBox.ConnectReturnPressed(func() {
		if len(rt.enterBox.Text()) == 0 {
			return
		}

		msg := rt.enterBox.Text()

		if user.profile == nil {
			return
		}

		b := new(bytes.Buffer)
		fmt.Fprintf(b, "*%s*: %s", user.profile, msg)
		user.profile.SendPacket(core.IDPacketRoomChat, rt.room.UUID, b.Bytes())
		rt.enterBox.SetText("")
	})

	rt.messageBox.SetOpenLinks(false)
	rt.messageBox.SetOpenExternalLinks(false)
	rt.messageBox.ConnectAnchorClicked(func(link *qt.QUrl) {
		gui.QDesktopServices_OpenUrl(link)
	})
	room.SetHandler(rt)

	return rt
}

// addRoomTab adds a new room to the list of tabs.
func addRoomTab(room *core.Room) error {
	if room == nil {
		return errors.New("room was nil")
	}

	roomTab := NewRoomTab(room)
	user.Archive.AddPacketHandler(room.UUID.String(), roomTab)
	centralWidget := widgets.NewQWidget(window, 0)

	centralWidget.SetLayout(roomTab.holder)
	roomTabPane.AddTab(centralWidget, roomTab.room.Name)

	if currentTab == nil {
		currentTab = roomTab
	}

	roomTabs = append(roomTabs, currentTab)

	return nil
}

// Send is just a raw print to the message box.
func (rt *RoomTab) Send(packet *core.Packet) error {
	if packet == nil {
		return errors.New("packet was nil")
	}

	rt.messageBox.Append(packet.String())

	return nil
}

// Receive is for trying to delegate what the packet type is and handling it.
func (rt *RoomTab) Receive(packet *core.Packet) error {
	if packet == nil {
		return errors.New("packet was nil")
	}

	switch packet.PacketType {
	case core.IDPacketChat:
		if rt.messageBox == nil {
			return errors.New("messageBox was nil")
		}

		time := "<i>" + time.Now().Format(core.SystemSettings.TimeExample) + "</i> - "
		rt.messageBox.Append(time + MarkupToHTML(packet.String()))
		AddSound(SoundChatMessage)
		return nil
	case core.IDPacketVoice:
		GetPacket(packet)
		return nil
	}

	return errors.New("packet type not recognized")
}

package ui

import (
	"fmt"
	"html"
	"regexp"
	"strings"
)

func tagLinks(msg string) string {

	regex, err := regexp.Compile(`(((www\.|https?:\/\/)([0-z]|\.|\/)+)|([0-z]+\.(info|io|net|org|com|co\.uk)+))`)

	if err != nil {
		fmt.Println(err.Error())
		return msg
	}

	linkIndexes := regex.FindAllStringIndex(msg, -1)

	if linkIndexes == nil {
		return msg
	}

	var ret string

	lastIndex := 0
	for _, i := range linkIndexes {
		ret += msg[lastIndex:i[0]]
		ret += "<a href="
		ret += msg[i[0]:i[1]]
		ret += ">"
		ret += msg[i[0]:i[1]]
		ret += "</a> "
		lastIndex = i[1]
	}
	ret += msg[lastIndex:len(msg)]

	return ret

}

func applyMarkup(input, chk, tag string) string {

	if strings.Contains(input, chk) {
		var retString string

		regex := regexp.MustCompile(`\` + chk + `.+` + `\` + chk)

		indices := regex.FindAllStringIndex(input, -1)
		if indices == nil {
			return input
		}

		lastIndex := 0
		for _, i := range indices {
			retString += input[lastIndex:i[0]]
			retString += "<" + tag + ">"
			retString += input[i[0]+1 : i[1]-1]
			retString += "</" + tag + ">"
			lastIndex = i[1]
		}
		retString += input[lastIndex:len(input)]
		return retString
	}

	return input
}

//MarkupToHTML is used to convert markup to HTML
func MarkupToHTML(msg string) (htmlOut string) {
	htmlOut = html.EscapeString(msg)
	htmlOut = tagLinks(htmlOut)
	htmlOut = applyMarkup(htmlOut, "*", "b")
	htmlOut = applyMarkup(htmlOut, "_", "i")
	htmlOut = applyMarkup(htmlOut, "~", "s")

	return htmlOut
}

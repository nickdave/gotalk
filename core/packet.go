package core

import (
	"bytes"
	"errors"
	"fmt"
	"time"

	"github.com/golang/glog"
	"github.com/google/uuid"
)

func init() {
	// make this determine this automatically
	PacketHeaderSize = 0

	r, _ := uuid.NewRandom()
	packet, _ := NewPacket(IDPacketInvalid, r, r, []byte(""))
	data, _ := packet.MarshalBinary()
	PacketHeaderSize = len(data)
	MaxPacketBodySize = MaxPacketSize - PacketHeaderSize
	//fmt.Printf("PacketHeaderSize: %d\nMaxPacketBodySize: %d\n", PacketHeaderSize, MaxPacketBodySize)
}

func min(a, b int) int {
	if a < b {
		return a
	}

	return b
}

func max(a, b int) int {
	if a > b {
		return a
	}

	return b
}

// Clamp clamps the value between min and max.
func clamp(value int, mn int, mx int) int {
	return min(max(value, mn), mx)
}

// PacketHeaderSize is the size of the header on the packet
var PacketHeaderSize = 0

// MaxPacketBodySize is the maximum size of the body of the packet
var MaxPacketBodySize = 0

// MaxPacketSize is the maximum size for a packet, in bytes.
// MaxBufferSize is the maximum size of the packet buffer. The unit is packets, so the maximum size in bytes would be MaxPacketSize*MaxBufferSize.
// PacketTimeout is the number of seconds that a packet should be attempted to be processed before being dropped
const (
	MaxPacketSize = 1024
	MaxBufferSize = 65536
	PacketTimeout = 5
)

// PacketBuffer is a slice of packet pointers
type PacketBuffer []*Packet

// PacketType is used for Packet identifiers.
type PacketType int

/*
IDPacketConnection marks a new client connection.
IDPacketDisconnection marks a client disconnection.
IDPacketChat marks packet to be displayed to the clients as a chat message.
IDPacketVoice marks a packet to be read as voice data.
IDPacketRoomChat marks the packet to be routed by a room.
IDPacketRoomVoice marks the packet to be routed by a room.
*/
const (
	IDPacketInvalid PacketType = iota
	IDPacketMultilength
	IDPacketConnection
	IDPacketDisconnection
	IDPacketChat
	IDPacketVoice
	IDPacketRoomChat
	IDPacketRoomVoice
	IDPacketRoomCreation
	IDPacketRoomDeletion
	IDPacketArchive
)

func (t *PacketType) String() string {
	switch *t {
	case IDPacketInvalid:
		return "IDPacketInvalid"
	case IDPacketMultilength:
		return "IDPacketMultilength"
	case IDPacketConnection:
		return "IDPacketConnection"
	case IDPacketDisconnection:
		return "IDPacketDisconnection"
	case IDPacketChat:
		return "IDPacketChat"
	case IDPacketVoice:
		return "IDPacketVoice"
	case IDPacketRoomChat:
		return "IDPacketRoomChat"
	case IDPacketRoomVoice:
		return "IDPacketRoomVoice"
	case IDPacketRoomCreation:
		return "IDPacketRoomCreation"
	case IDPacketRoomDeletion:
		return "IDPacketRoomDeletion"
	case IDPacketArchive:
		return "IDPacketArchive"
	default:
		return "Unknown"
	}
}

// Packet is the basis of our network communication. Everything to be sent over the network is broken into packets.
type Packet struct {
	PacketType   PacketType // Type of Packet
	DestUUID     uuid.UUID  // UUID of the destination
	ConnUUID     uuid.UUID  // UUID of the connection that sent this Packet
	OrigUUID     uuid.UUID  // UUID of the connection that this packet originated on
	PacketUUID   uuid.UUID  // UUID of this Packet
	Data         []byte     // Content of the Packet
	creationTime time.Time  // When this packet was created on this system
}

// NewPacket for creating a new 'instance' of the Packet struct
// Packet header is 76 bytes
func NewPacket(t PacketType, dest uuid.UUID, origin uuid.UUID, data []byte) (*Packet, error) {
	id, _ := uuid.NewRandom()
	return &Packet{t,
		dest,
		origin,
		origin,
		id,
		data,
		time.Now()}, nil
}

// StitchPacket stiches multiple individual packets into one. The PacketType, DestUUID, OrigUUID, ConnUUID, and PacketUUID are determined by the last packet in the slice.
func StitchPacket(packets PacketBuffer) (*Packet, error) {
	if packets == nil {
		return nil, errors.New("packet slice was nil")
	}

	packet := new(Packet)

	for _, p := range packets {
		if p == nil {
			continue
		}

		packet.Data = append(packet.Data, p.Data...)

		if p.PacketType != IDPacketMultilength {
			packet.PacketType = p.PacketType
			packet.DestUUID = p.DestUUID
			packet.ConnUUID = p.ConnUUID
			packet.OrigUUID = p.OrigUUID
			packet.PacketUUID = p.PacketUUID
		}
	}

	if packet.PacketType == IDPacketMultilength {
		return nil, errors.New("not a valid packet")
	}

	packet.creationTime = time.Now()
	return packet, nil
}

// Split takes a large packet and splits it into multiple packets that are under MaxPacketSize length. Leaves the original packet untouched.
func (p *Packet) Split() (PacketBuffer, error) {
	bufferSize := int((len(p.Data) / MaxPacketBodySize))
	glog.Info(fmt.Sprintf("splitting packet into %d packets", bufferSize))
	packets := make(PacketBuffer, bufferSize)

	var packet *Packet

	for i := 0; i < len(p.Data); i += MaxPacketBodySize {
		data := p.Data[i:min(i+MaxPacketBodySize, len(p.Data))]

		packet, _ = NewPacket(IDPacketMultilength, p.DestUUID, p.OrigUUID, data)
		packet.PacketUUID = p.PacketUUID // Split packets all have the same ID

		packets = append(packets, packet)
	}

	packet.PacketType = p.PacketType // Modifying our last packet with the original type

	return packets, nil
}

// ReadPacket is used to deserialize a byte array into a new packet object.
func ReadPacket(data []byte) (*Packet, error) {
	if data == nil {
		return nil, errors.New("data is nil")
	}

	packet := new(Packet)
	err := packet.UnmarshalBinary(data)
	if err != nil {
		glog.Error(err)
		return nil, nil
	}

	return packet, nil
}

// String returns the packet data as a string.
func (p *Packet) String() string {
	return string(p.Data)
}

// StringDebug returns a string formatted with the packet header.
func (p *Packet) StringDebug() string {
	return fmt.Sprintf("Type: %s\nPacketUUID: %s\nOrigUUID: %s\nConnUUID: %s\nDestUUID: %s\nData: %s\n", p.PacketType.String(), p.PacketUUID, p.OrigUUID, p.ConnUUID, p.DestUUID, p.Data)
}

// MarshalBinary serializes the packet.
func (p *Packet) MarshalBinary() ([]byte, error) {
	var b bytes.Buffer

	_, err := fmt.Fprint(&b, p.PacketType, p.DestUUID, p.ConnUUID, p.OrigUUID, p.PacketUUID)

	if err != nil {
		glog.Error(err)
		return nil, nil
	}

	// Total length of our byte stream
	lengthRaw := len(b.Bytes())
	lengthRaw += len(p.Data) + 1 // Adding one for space between header and body

	d := make([]byte, 0, lengthRaw)

	d = append(d, b.Bytes()...)
	d = append(d, 0x20) // Putting a space between our header and body
	d = append(d, p.Data...)

	// Length of the finalized packet, clamped between 0 and MaxPacketSize
	lengthFinal := clamp(len(d), 0, MaxPacketSize)

	return d[0:lengthFinal], nil
}

// UnmarshalBinary deserializes a packet.
func (p *Packet) UnmarshalBinary(data []byte) error {
	if data == nil {
		return errors.New("data is nil")
	}

	if len(data) <= 0 {
		return errors.New("data is empty")
	}

	b := bytes.NewBuffer(data)

	// We have to read UUIDs into byte arrays first because Fscan can't read directly into a UUID
	dUUID := make([]byte, 0, 16) // destination
	cUUID := make([]byte, 0, 16) // connection
	oUUID := make([]byte, 0, 16) // origin
	pUUID := make([]byte, 0, 16) // packet

	_, err := fmt.Fscan(b, &p.PacketType, &dUUID, &cUUID, &oUUID, &pUUID)

	if err != nil {
		glog.Error(err)
		return nil
	}

	cache, err0 := uuid.ParseBytes(dUUID)
	if err0 != nil {
		glog.Error(err0)
		return nil
	}
	p.DestUUID = cache

	cache0, err1 := uuid.ParseBytes(cUUID)
	if err1 != nil {
		glog.Error(err1)
		return nil
	}
	p.ConnUUID = cache0

	cache1, err1 := uuid.ParseBytes(oUUID)
	if err1 != nil {
		glog.Error(err1)
		return nil
	}
	p.OrigUUID = cache1

	cache2, err3 := uuid.ParseBytes(pUUID)
	if err3 != nil {
		glog.Error(err3)
		return nil
	}
	p.PacketUUID = cache2

	// Getting the starting location of our data. Adds 1 for the space padding.
	indexStart := (len(data) + 1) - len(b.Bytes())
	if indexStart >= len(data) {
		return nil
	}

	// Reading the data from the packet
	p.Data = data[indexStart:]
	p.creationTime = time.Now()

	return nil
}

// Empty checks whether the packet has any data.
func (p *Packet) Empty() bool {
	return len(p.Data) <= 0
}

// Timeout checks whether the timeout period for this packet has passed.
func (p *Packet) Timeout() bool {
	return time.Since(p.creationTime).Seconds() > PacketTimeout
}

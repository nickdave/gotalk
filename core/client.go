package core

import (
	"encoding/binary"
	"errors"
	"fmt"
	"net"

	"github.com/golang/glog"
	"github.com/google/uuid"
)

// MaxClients is the max number of clients on a server
const (
	MaxClients = 65536
)

// Client is a Connection, simply containing a username and the network connection.
type Client struct {
	conn           net.Conn
	datastream     chan []byte
	inputPackets   chan *Packet               // Packets waiting to be processed
	outputPackets  chan *Packet               // Packets waiting to be sent
	parkingPackets map[uuid.UUID]PacketBuffer // Used by mulit-packet messages waiting for the rest of the data to arrive
	phandler       PacketHandler
	uhandler       ProfileHandler
	Profile        *Profile
	processing     bool
}

// NewClient is essentially a constructor
func NewClient(conn net.Conn, phandler PacketHandler, uhandler ProfileHandler) (*Client, error) {
	if phandler == nil {
		return nil, errors.New("PacketHandler was nil")
	}

	return &Client{conn,
		make(chan []byte, MaxBufferSize),
		make(chan *Packet, MaxBufferSize),
		make(chan *Packet, MaxBufferSize),
		make(map[uuid.UUID]PacketBuffer),
		phandler,
		uhandler,
		nil,
		false}, nil
}

// Close is used to safely release the client
func (c *Client) Close() {
	c.conn.Close()
}

// SendData is for sending a raw byte array.
func (c *Client) SendData(data []byte) error {
	length := len(data)
	if length <= 0 {
		return errors.New("data is empty")
	}

	b := make([]byte, 4)
	binary.LittleEndian.PutUint32(b, uint32(length))

	// Send the packet
	_, err := c.conn.Write(b)
	if err != nil {
		return nil
	}
	c.conn.Write(data)

	return nil
}

// Send sends the given packet across the network.
func (c *Client) Send(packet *Packet) error {
	if packet == nil {
		return errors.New("given packet was nil")
	}

	glog.Info(fmt.Sprintf("client sending a packet of type %s", packet.PacketType.String()))

	packet.ConnUUID = c.UUID() // Updates the packet with the most recent connection, aka this one

	// Send the packet
	d, err := packet.MarshalBinary()
	if err != nil {
		glog.Warning(err)
		return nil
	}

	c.SendData(d)
	return nil
}

// SendQueue queues the given packet to be sent. It will split large packets into smaller packets and queue them.
func (c *Client) SendQueue(packet *Packet) error {
	if packet == nil {
		return errors.New("packet was nil")
	}

	if len(packet.Data) > MaxPacketBodySize {
		packets, err := packet.Split()
		if err != nil {
			glog.Error(err)
			return nil
		}

		for _, p := range packets {
			c.outputPackets <- p
		}
	} else {
		c.outputPackets <- packet
	}

	return nil
}

// SendPacket creates a new Packet with the given data and sends it.
func (c *Client) SendPacket(t PacketType, dest uuid.UUID, data []byte) error {
	packet, err := NewPacket(t, dest, c.UUID(), data)
	if err != nil {
		glog.Warning(err)
		return nil
	}

	c.SendQueue(packet)

	return nil
}

// Receive evaluates the given packet and sends it to the correct handler.
func (c *Client) Receive(packet *Packet) error {
	if packet == nil {
		return errors.New("given packet was nil")
	}

	glog.Info(fmt.Sprintf("client processing a packet of type %s", packet.PacketType.String()))

	//glog.Info(packet.StringDebug())

	parked := c.parkingPackets[packet.PacketUUID]
	if parked != nil {
		c.addParking(packet)
		return nil
	}

	switch packet.PacketType {
	case IDPacketMultilength:
		c.addParking(packet)
	default:
		if c.phandler == nil {
			return errors.New("client's PacketHandler was nil")
		}
		glog.Info("client sent a packet to its handler")
		c.phandler.Receive(packet)
	}

	return nil
}

// addParking adds a packet in waiting to the parkingPackets map. Will recompile packets that have all their data and add them to inputPackets.
func (c *Client) addParking(packet *Packet) error {
	if packet == nil {
		return errors.New("packet was nil")
	}

	// Getting the location of the PacketBuffer for this PacketUUID, if there is one
	packets := c.parkingPackets[packet.PacketUUID]
	if packets == nil {
		packets = make(PacketBuffer, 10) // TODO random length number, please change
		glog.Info(fmt.Sprintf("adding a new parking packet %s", packet.PacketUUID))
	} else {
		glog.Info(fmt.Sprintf("adding to an existing parking packet %s", packet.PacketUUID))
	}

	packets = append(packets, packet)

	if packet.PacketType == IDPacketMultilength {
		c.parkingPackets[packet.PacketUUID] = packets
		glog.Info(fmt.Sprintf("storing parking packet %s", packet.PacketUUID))
	} else {
		glog.Info(fmt.Sprintf("stiching parking packet %s", packet.PacketUUID))
		p, err := StitchPacket(packets)
		if err != nil {
			glog.Error(err)
			return nil
		}
		glog.Info(fmt.Sprintf("adding parking packet %s to inputPackets", packet.PacketUUID))
		delete(c.parkingPackets, p.PacketUUID)
		c.AddInputPacket(p)
	}

	return nil
}

// AddInputPacket adds the given packet to the input queue.
func (c *Client) AddInputPacket(packet *Packet) error {
	if packet == nil {
		return errors.New("packet was nil")
	}

	c.inputPackets <- packet

	return nil
}

// SetProcessing is used to set whether this client is currently processing the network
func (c *Client) SetProcessing(process bool) {
	c.processing = process
}

// Update handles the regular server updates. Needs to be called externally.
func (c *Client) Update() error {
	defer glog.Flush() // Print our errors

	// Sends out packets in queue
	select {
	case packet := <-c.outputPackets:
		c.Send(packet)
	default:
	}

	// Processes the datastream
	select {
	case data := <-c.datastream:
		go c.ProcessData(data)
	default:
	}

	// Processes the packets
	select {
	case packet := <-c.inputPackets:
		if !packet.Timeout() {
			go c.Receive(packet)
		} else {
			glog.Warning("dropping packet after packet timeout")
		}
	default:
	}

	// Reads data after processing to help prevent packets going out of order
	if !c.processing {
		go c.PollNet()
	}

	return nil
}

// PollNet reads a set of data from the network and adds it to the datastream.
func (c *Client) PollNet() error {
	c.SetProcessing(true)
	defer c.SetProcessing(false)

	data, err := c.GetData()
	if err != nil {
		// Need to nest to make sure err isnt nil
		if err.Error() == "EOF" {

			c.uhandler.Disconnect(c.Profile)
			return nil
		}
		glog.Error(err)
		return nil
	}

	c.datastream <- data
	return nil
}

// ProcessData processes a byte slice into packet and recieves it
func (c *Client) ProcessData(data []byte) error {
	packet, err := ReadPacket(data)
	if err != nil {
		glog.Error(err)
		return nil
	}

	glog.Info(fmt.Sprintf("client read a packet of type %s", packet.PacketType.String()))

	c.AddInputPacket(packet)

	return nil
}

// GetData is used to read raw data from the network connection
func (c *Client) GetData() ([]byte, error) {
	var lengthBytes [4]byte

	_, err := c.conn.Read(lengthBytes[0:])
	if err != nil {
		if err.Error() == "EOF" {
			return nil, err
		}

		glog.Error(err)
		return nil, nil
	}

	length := binary.LittleEndian.Uint32(lengthBytes[:])

	if length > MaxPacketSize {
		length = MaxPacketSize
	}

	var data [MaxPacketSize]byte

	_, err = c.conn.Read(data[0:length])
	if err != nil {
		glog.Error(err)
		return nil, nil
	}

	if length <= 0 {
		return nil, nil
	}

	return data[:length], nil
}

// UUID is used to get the UUID of the profile.
func (c *Client) UUID() uuid.UUID {
	if c.Profile == nil {
		id, _ := uuid.NewRandom()
		return id
	}

	return c.Profile.UUID
}

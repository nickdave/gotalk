package core

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/golang/glog"
	"github.com/google/uuid"
)

// MinUsernameSize is the minimum size for a username
// MaxUsernameSize is the maximum size for a username
const (
	systemSettingsFileName = "settings.json"
	MinUsernameSize        = 4
	MaxUsernameSize        = 32
)

//CurrentHash is the hash of the server you're currently connected to
var CurrentHash string

// NewProfile is essentially a constructor
func NewProfile(username, description, location, visibility, avatar, stylesheetDir, fontDir, password, filesDir string, id uuid.UUID, client *Client) (*Profile, error) {
	return &Profile{username,
		description,
		location,
		visibility,
		avatar,
		id,
		client}, nil
}

//LoadProfile is used to retrieve a saved user profile.
func LoadProfile(filename string) (*Profile, error) {
	filepath := filepath.Join(SystemSettings.FilesDirectory, "./profiles/", filename+".json")

	fileContents, err := ioutil.ReadFile(filepath)
	if err != nil {
		glog.Error(err)
		return nil, fmt.Errorf("user profile not found for %s", filename)
	}

	profile := new(Profile)
	err = json.Unmarshal([]byte(fileContents), profile)

	if err != nil {
		glog.Error(err)
		return nil, nil
	}

	glog.Info(filename, " profile loaded successfully")

	return profile, nil
}

//Profile is a struct that contains the data corresponding to a specific user
type Profile struct {
	Username    string
	Description string
	Location    string
	Visibility  string
	Avatar      string
	UUID        uuid.UUID
	Client      *Client `json:"-"`
}

// Save is used to save the profile to a given filename.
func (u *Profile) Save(filename string) error {
	if filename == "" {
		return errors.New("filename was not given")
	}

	data, err := json.Marshal(*u)
	if err != nil {
		glog.Error("unable to marshal data into json:", err.Error())
		return nil
	}

	filepath := filepath.Join(SystemSettings.FilesDirectory, "./profiles/", filename+".json")
	file, err := os.Create(filepath)
	defer file.Close()

	if err != nil {
		glog.Error(err.Error())
		return nil
	}

	file.Write(data)

	return nil
}

func (u *Profile) String() string {
	if len(u.Username) > 0 {
		return u.Username
	}

	return "INVALID"
}

// TODO make these setters send an update to the server

// SetUsername sets the username of this profile
func (u *Profile) SetUsername(set string) error {
	if len(set) < MinUsernameSize {
		return fmt.Errorf("username must be at least %d characters long", MinUsernameSize)
	}

	if len(set) > MaxUsernameSize {
		return fmt.Errorf("username must be less than %d characters long", MaxUsernameSize)
	}

	u.Username = set
	return nil
}

// SetDescription sets the description of this profile
func (u *Profile) SetDescription(set string) error {
	u.Description = set
	return nil
}

// SetLocation sets the location of this profile
func (u *Profile) SetLocation(set string) error {
	u.Location = set
	return nil
}

// SetAvatar sets the avatar of this profile
func (u *Profile) SetAvatar(set string) error {
	u.Avatar = set
	return nil
}

// SetVisibility sets the visibility of this profile
func (u *Profile) SetVisibility(set string) error {
	u.Visibility = set
	return nil
}

// SetClient sets the client connection of this profile
func (u *Profile) SetClient(client *Client) error {
	if client == nil {
		return errors.New("client was nil")
	}

	// TODO fix this so its non-cyclical
	client.Profile = u
	u.Client = client

	return nil
}

// Send puts the given packet in the sendqueue of the client
func (u *Profile) Send(packet *Packet) error {
	if u.Client == nil {
		return errors.New("profile client was nil")
	}

	if packet == nil {
		return errors.New("packet was nil")
	}

	u.Client.SendQueue(packet)
	return nil
}

// Receive sends the given packet to the client for processing.
func (u *Profile) Receive(packet *Packet) error {
	if u.Client == nil {
		return errors.New("profile client was nil")
	}

	if packet == nil {
		return errors.New("packet was nil")
	}

	u.Client.Receive(packet)
	return nil
}

// SendPacket is used to create and send a packet through client.
func (u *Profile) SendPacket(t PacketType, dest uuid.UUID, data []byte) error {
	if u.Client == nil {
		return errors.New("profile client was nil")
	}

	u.Client.SendPacket(t, dest, data)
	return nil
}

// Close closes the client connection.
func (u *Profile) Close() {
	if u.Client != nil {
		u.Client.Close()
		u.Client = nil
	}
}

// Update is used for regular profile updates.
func (u *Profile) Update() error {
	if u.Client != nil {
		u.Client.Update()
	}

	return nil
}

// AddInputPacket adds the packet to the client input packets queue.
func (u *Profile) AddInputPacket(packet *Packet) error {
	if u.Client == nil {
		return errors.New("client was nil")
	}

	return u.Client.AddInputPacket(packet)
}

package main

import (
	"gotalk/core/ui"

	"github.com/therecipe/qt/widgets"
)

func main() {
	ui.InitializeUI()

	widgets.QApplication_Exec()
}
